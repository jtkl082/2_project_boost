﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    Rigidbody rigidBody;
    AudioSource audioData;

    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float mainThrust = 100f;
    [SerializeField] float levelLoadDelay = 2f;

    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip successSound;
    [SerializeField] AudioClip deathSound;

    [SerializeField] ParticleSystem mainEngineParticles;
    [SerializeField] ParticleSystem successParticles;
    [SerializeField] ParticleSystem deathParticles;

    bool isTransitioning = false; 
    
    // enum State {Alive, Dying, Transcending} -overcoded, changed to bool isTransitioning
    // State state = State.Alive;

    bool collisionDisabled = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioData = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isTransitioning) // stop sound on death
        {
            RespondToThrustInput();
            RespondToRotateInput();
            ResetLevel();
        }
        // only if in debug mode
        if (Debug.isDebugBuild)
        {
            DebugKeys();
        }
    }

    private void ResetLevel()
    {
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void RespondToThrustInput()
    {
        if (Input.GetKey(KeyCode.Space)) // able to thrust while rotating
        {
            ApplyThrust();
        }
        else
            StopApplyingThrust();
    }

    private void StopApplyingThrust()
    {
        audioData.Stop();
        mainEngineParticles.Stop();
    }

    private void ApplyThrust()
    {
        rigidBody.AddRelativeForce(Vector3.up * mainThrust);
        if (!audioData.isPlaying) // so sound does not layer
        {
            audioData.PlayOneShot(mainEngine);
            mainEngineParticles.Play();
        }
    }

    private void RespondToRotateInput()
    {
        rigidBody.freezeRotation = true; // take manual control of rotation
                
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rcsThrust);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(-Vector3.forward * rcsThrust);
        }

        rigidBody.freezeRotation = false; // resume physics control of rotation
    }

    void OnCollisionEnter(Collision collision)
    {
        if (isTransitioning || collisionDisabled) { return; } // ignore collisions


        switch (collision.gameObject.tag)
        {
            case "Friendly":
                // do nothing
                break;
            case "Enemy":
                // dead
                print("dead");
                StartDeathSequence();
                break;
            case "Finish":
                print("Finish"); // to do: remove
                StartSuccessSequence();
                break;
            default:
                Debug.LogError("Inavlid"); //catchall
                break;
        }
    }

    private void LoadFirstLevel()
    {
        // state = State.Alive; -obsolete
        SceneManager.LoadScene(0); // maybe add a restart level cmd
    }

    private void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0; // loop back to start
        }
         SceneManager.LoadScene(nextSceneIndex); // todo allow more than 2 levels
    }

    private void StartDeathSequence()
    {
        isTransitioning = true;
        audioData.Stop();
        audioData.PlayOneShot(deathSound);
        deathParticles.Play();
        Invoke("Respawn", levelLoadDelay);
    }

    private void StartSuccessSequence()
    {
        isTransitioning = true;
        audioData.Stop();
        audioData.PlayOneShot(successSound);
        successParticles.Play();
        Invoke("LoadNextLevel", levelLoadDelay); // paremeterise time
    }

    private void Respawn()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void DebugKeys()
    {
        if (Input.GetKey(KeyCode.L))
        {
            LoadNextLevel();
        }
        else if (Input.GetKey(KeyCode.C))
        {
            // toggle collision
            collisionDisabled = !collisionDisabled;
        }
    }
}

